function calculateTip(billAmount) {
    var percentage;
    if(billAmount < 50 ) {
        percentage = 0.2;
    } else if(billAmount >=50 && billAmount < 200) {
        percentage = 0.15;
    } else {
        percentage = 0.1;
    }

    return percentage * billAmount;

}

console.log(calculateTip(10));

var bills = [124, 48, 268];

var tipArray = [calculateTip(bills[0]), calculateTip(bills[1]), calculateTip(bills[2])];

console.log(tipArray);

var finalAmount = [bills[0] + tipArray[0], 
                bills[1] + tipArray[1],
            bills[2] + tipArray[2]];

console.log(finalAmount);
