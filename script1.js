var mark = {
    fullName : 'Mark Williams',
    mass : 163,
    height: 1.45,
    calcBMI: function() {
        this.BMI = this.mass / (this.height
            * this.height)
            return this.BMI
    }
};

var john = {
    fullName : 'John Smith',
    mass : 114,
    height: 1.45,
    calcBMI: function() {
        this.BMI = this.mass / (this.height
            * this.height)
            return this.BMI
    }
};

if(mark.calcBMI() > john.calcBMI()) {
    console.log(mark.fullName + ' has highest BMI of ' + mark.BMI);
} else if(john.BMI > mark.BMI) {
    console.log(john.fullName + ' has highest BMI of ' + john.BMI);
} else {
    console.log(mark.fullName + ' and ' + john.fullName + ' have same BMI.');
}